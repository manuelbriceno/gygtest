##README for GetYourGuide iOS Developer test

This sample project is for evaluation purposes for GetYourGuide tech team. Is a simple test using basic iOs knowledge.

**Third party libraries used in this project**

1. [raulriera/Cacher](https://github.com/raulriera/Cacher): For the cache proccesing
2. [hsousa/HCSStarRatingView](https://github.com/hsousa/HCSStarRatingView): For the rating stars view

---

**ToDo**

1. Dependency Injection for unit testing.
2. Visualy handle the errors.
3. Caching all the pages already loaded.
