//
//  APIService.swift
//  Test
//
//  Created by Manuel Briceno on 05/07/2018.
//  Copyright © 2018 Luis Manuel Briceño. All rights reserved.
//

import Foundation
import Cachable

let getURL: String = "https://www.getyourguide.com/berlin-l17/tempelhof-2-hour-airport-history-tour-berlin-airlift-more-t23776/reviews.json?count=5&type=&sortBy=date_of_review&direction=DESC"

let postURL: String = "http://localhost:1234/api/review/new"

class ReviewsService {
    
    //Use params to filter the request ["page":0, "rating":0]
    static func GET(params:Dictionary<String, Any>?, complete: @escaping ( _ success: Bool, _ reviewsData: ReviewsData?)->() ) {
        var url = getURL
        if let queryString = params?.queryString {
            url = "\(getURL)&\(queryString)"
        }
        guard let reviewsURL = URL(string: url) else {
            complete(false, nil)
            return
        }
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        URLSession.shared.dataTask(with: reviewsURL) { (data, response
            , error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let data = data else {
                    complete(false, nil)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    let reviewsData = try decoder.decode(ReviewsData.self, from: data)
                    //We only save the first page in cache.
                    if let page = params?["page"] as? Int {
                        if page == 0 {
                            Cacher(destination: .temporary).persist(item: reviewsData) { url, error in
                                if let error = error {
                                    print("Data failed to persist: \(error)")
                                } else {
                                    print("Data persisted in \(String(describing: url))")
                                }
                            }
                        }
                    }
                    complete(true, reviewsData)
                } catch let err {
                    print("Err", err)
                    complete(false, nil)
                }
            }
        }.resume()
    }
    
    static func POST(params:Dictionary<String, Any>, complete: @escaping ( _ success: Bool, _ review: Review?)->() ) {
        guard let addURL = URL(string: postURL) else {
            complete(false, nil)
            return
        }
        var request = URLRequest(url: addURL)
        request.httpMethod = "POST"
        request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            complete(false, nil)
            return
        }
        request.httpBody = httpBody
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                guard let data = data else {
                    complete(false, nil)
                    return
                }
                do {
                    let decoder = JSONDecoder()
                    //We decode what is supposed to be the review response
                    let review = try decoder.decode(Review.self, from: data)
                    complete(true, review)
                    
                } catch let err {
                    print("Err", err)
                    complete(false, nil)
                }
            }
        }.resume()
    }
}
