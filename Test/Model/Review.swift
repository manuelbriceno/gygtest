//
//  ReviewModel.swift
//  Test
//
//  Created by Manuel Briceno on 05/07/2018.
//  Copyright © 2018 Luis Manuel Briceño. All rights reserved.
//

import Foundation
import Cachable

let CACHE_NAME = "ReviewsData"

struct ReviewsData: Cachable, Codable {
    var fileName: String {
        return CACHE_NAME
    }
    let data: [Review]
    let total_reviews_comments: Int
    let status: Bool
    let message: String?
    
    //We need to specify the CodingKeys to exclude fileName var in order to conform the Cachable Protocol and the server response.
    private enum CodingKeys: String, CodingKey {
        case data
        case total_reviews_comments
        case status
        case message
    }
}

struct Review: Codable {
    let review_id: Int
    let rating: String 
    let title: String?
    let message: String
    let author: String
    let date: String
    let languageCode: String
    let traveler_type: String?
    let reviewerName: String
    let reviewerCountry: String
}
