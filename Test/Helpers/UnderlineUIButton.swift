//
//  UnderlineUIButton.swift
//  Test
//
//  Created by Manuel Briceno on 06/07/2018.
//  Copyright © 2018 Luis Manuel Briceño. All rights reserved.
//

import Foundation
import UIKit

//Subclass for underlining
class UnderlineUIButton: UIButton {
    var underline: Bool = false {
        didSet {
            let attributes : [NSAttributedStringKey : Any] = [
                NSAttributedStringKey.underlineStyle : underline ? NSUnderlineStyle.styleSingle.rawValue : NSUnderlineStyle.styleNone.rawValue
            ]
            let attributedString = NSAttributedString(string: self.currentTitle!, attributes: attributes)
            self.setAttributedTitle(attributedString, for: .normal)
        }
    }
}
