//
//  ViewController.swift
//  Test
//
//  Created by Manuel Briceno on 05/07/2018.
//  Copyright © 2018 Luis Manuel Briceño. All rights reserved.
//

import UIKit
import Cachable

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var reviewsCountLabel: UILabel!
    @IBOutlet weak var enLanguageButton: UnderlineUIButton!
    @IBOutlet weak var deLanguageButton: UnderlineUIButton!
    
    var page = 0;
    var reviews = [Review]()
    var filtered = [Review]()
    var loadingMore = true
    var filtering = false
    var languageCode = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 155
        
        //Refer to Cachable documentation here: https://github.com/raulriera/Cacher
        if let reviewsData: ReviewsData = Cacher(destination: .temporary).load(fileName: CACHE_NAME) {
            setUpData(reviewsData: reviewsData)
            print("Loaded from the cache")
        }
        ReviewsService.GET(params: ["page": page], complete: { (success, reviewsData) in
            if success {
                self.setUpData(reviewsData: reviewsData!)
                print("Loaded from the url")
            }
        })
    }
    
    func setUpData(reviewsData: ReviewsData) {
        self.reviews += reviewsData.data
        self.reviewsCountLabel.text = "Reviews (\(reviewsData.total_reviews_comments))"
        self.loadingMore = false
        if filtering {
            self.filtered = reviews.filter { $0.languageCode == languageCode }
        }
        self.tableView.reloadData()
        
    }
    
    // MARK: - Filter language functions
    @IBAction func filterLanguage(sender: UIView) {
        if sender.tag == 1 {
            if languageCode == "de" {
                clearLanguageFilter()
            }else {
                languageCode = "de"
                activateLanguageFilter()
            }
        }else {
            if languageCode == "en" {
                clearLanguageFilter()
            }else {
                languageCode = "en"
                activateLanguageFilter()
            }
        }
        tableView.reloadData()
    }
    
    func clearLanguageFilter() {
        languageCode = ""
        filtering = false
        deLanguageButton.underline = false
        enLanguageButton.underline = false
    }
    
    func activateLanguageFilter() {
        filtering = true
        if languageCode == "de" {
            deLanguageButton.underline = true
            enLanguageButton.underline = false
        }else {
            deLanguageButton.underline = false
            enLanguageButton.underline = true
        }
        self.filtered = reviews.filter { $0.languageCode == languageCode }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Navigation
    @IBAction func unwindFromNewReview(_ sender: UIStoryboardSegue) {
        
        if sender.source is NewReviewTableViewController {
            if let senderVC = sender.source as? NewReviewTableViewController {
                //We get the data from the modal and then add the review
                let params: [String : Any] = [
                    "rating": senderVC.ratingStarRatingView.value,
                    "title": senderVC.titleTextField.text!,
                    "reviewerName": senderVC.nameTextField.text!,
                    "message": senderVC.messageTextView.text!,
                    "languageCode": NSLocale.current.languageCode!
                    ]
                ReviewsService.POST(params: params, complete: { (success, review) in
                    if success {
                        if let review = review {
                            self.reviews.insert(review, at: 0)
                            self.tableView.reloadData()
                        }
                    }
                })
            }
        }
    }
}

// MARK: - TableView Delegate - DataSource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //We check if theres is a filtering option
        return filtering ? filtered.count : reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewsTableViewCell
        if let review: Review = filtering ? filtered[indexPath.row] : reviews[indexPath.row] {
            cell.dateLabel.text = review.date
            //We use an extesion to clean the html entities in the message
            cell.messageLabel.text = review.message.decodingHTMLEntities()
            cell.nameLabel.text = review.author
            cell.titleLabel.text = review.title
            cell.ratingImageView.image = UIImage(named:"rating_\(review.rating)")!
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = filtering ? filtered.count - 1 : reviews.count - 1
        if indexPath.row == lastElement {
            if !loadingMore {
                loadingMore = true
                page += 1
                ReviewsService.GET(params: ["page": page], complete: { (success, reviewsData) in
                    if(success) {
                        self.setUpData(reviewsData: reviewsData!)
                    }
                })
            }
        }
    }
}

// MARK: - ReviewsTableViewCell
class ReviewsTableViewCell: UITableViewCell {
    @IBOutlet weak var ratingImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
}

