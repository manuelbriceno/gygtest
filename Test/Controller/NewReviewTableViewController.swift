//
//  NewReviewTableViewController.swift
//  Test
//
//  Created by Manuel Briceno on 06/07/2018.
//  Copyright © 2018 Luis Manuel Briceño. All rights reserved.
//

import UIKit
import HCSStarRatingView

class NewReviewTableViewController: UITableViewController {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var ratingStarRatingView: HCSStarRatingView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func dissmissView(sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        if identifier == "UnwindFromNewReview" {
            if ratingStarRatingView.value == 0  {
                let alert = UIAlertController(title: "Oops!", message: "You need to rate the activity!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler:nil))
                self.present(alert, animated: true, completion: nil)
                return false
            }
        }
        return true
    }

}
